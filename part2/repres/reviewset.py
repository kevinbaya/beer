from review import *
from vector import *
from random import random
class ReviewSet(object):

    def __init__(self):
        self.rev_list = []
        self.rev_dict = {}

    def get_review(self, id):
        return self.rev_dict[id]
    
    def parse_vectors(self, input_file_name):
        """ Remplit la liste de revues avec celle dans le fichier .input
        Cette fonction remplit que l'id le vector
        """
        file = open(input_file_name,'r')

        for line in file:
            tokens = line.split(' ')
            if tokens[1] == 'review':
                rev = Review(tokens[0])
                for token in tokens[2:]:
                    [k,v] = token.split(':')
                    rev.vector[int(k)] = float(v)
                self.rev_list.append(rev)
                self.rev_dict[rev.id] = rev

    def parse_notes(self,output_file_name):
        file = open(output_file_name,'r')
        for line in file:
            tokens= line.split(' ')
            rev=self.get_review(tokens[0])
            for token in tokens[2:]:
                note = token.split(':')[1]
                rev.notes.append(int(note))

    def split_data(self, p):
        app_set = ReviewSet()
        test_set = ReviewSet()
        for rev in self.rev_list:
            if(random()<p):
                app_set.rev_list.append(rev)
                app_set.rev_dict[rev.id]=rev
            else:
                test_set.rev_list.append(rev)
                test_set.rev_dict[rev.id]=rev
        return app_set,test_set
