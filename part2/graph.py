from repres.recommenderdata import *
from numpy.linalg import norm
from threading import Thread
import time

def sim_cos(x,y):
	return np.dot(x,y)/(norm(x)*norm(y))

def create_sim_matrix(rm,f, seuil):
	"""
	Fonction general de creabt un graphe de similarite.
	Trop lent avec sim_cos.
	"""
	n, m = rm.shape
	sim_mat = np.zeros([n,n],dtype=bool)
	i=0
	while i<n:
		j=0
		while j<n:
			sim_mat[i,j] = f(rm[i,:],rm[j,:])>seuil
			j=j+1
		i=i+1
	return sim_mat

def create_sim_mat_cos(rating_mat):
	"""
	Creation d'un graphe avec une similarite cosinus.
	Version pure numpy plus rapide.
	Temps de calcul (Intel-Core2-Quad-Processor Q6600 : 2.4 Ghz)
	~30s sur donnees 'mini'
	~3 min sur donnees 'medium'

	@type rating_mat: Un numpy.array de float
	@param rating_mat: La matrice des notations:
	@type seuil: float
	@param seuil: Doit etre entre -1 et 1
"""
	# On calcul la norme de chaque vecteur utilisateur
	norm = np.sqrt((rating_mat * rating_mat).sum(axis=1)) 
	
	# On normalise chaque vecteur utilisateur
	rm = rating_mat/norm[:,np.newaxis]

	# On fait le produit scalaire de tous les couples d'utilisateurs possibles
	res = np.dot(rm,rm.T)
	return res

def create_graph_cos(rating_mat,seuil):
	return create_sim_mat_cos(rating_mat)>seuil

def wf(f,i,j,val):
	f.write('%d %d %d\n' % (i,j,val))

def wf_link(f,i,j,val):
	if val != 0:
		f.write('%d %d\n' % (i,j))

def save_graph(filename, graph, writing_fun):
	f = open(filename,'wb')
	n,m = graph.shape
	i = 0
	while i<n:
		j = 0
		while j<m:
			writing_fun(f,i,j,graph[i,j])
			j = j + 1
		i = i + 1
	f.close()
	
	


def clustering(sim_mat):
	n, m = sim_mat.shape
	clust = np.zeros(n)
	i=0
	while i<n:
		j=0
		du =0
		while j<n:
			if sim_mat[i,j] and i != j:
				du+=1.
			j+=1
		if du >2:
			subg = sim_mat[voisin,:][:,voisin]
			nb_voisin = subg.sum()/2.0;
			nb_lien = du *(du-1)/2.0
			clust[i] = nb_voisin/nb_lien
		else:
			clust[i]=0
		i+=1
		if i%1000 ==0:
			print i
	return clust
	


if __name__=="__main__":
	graph_data = get_graph_data('data/mini.relation','data/mini.output',4)
	cur = time.time()
	sim_mat = create_graph_cos(graph_data.rating_mat,0)
	#sim_mat = create_sim_matrix(graph_data.rating_mat,sim_cos,0.5)
	print "graph created"
	save_graph("/media/USB DISK/syrres/graph_cos_mini0_link.txt",sim_mat,wf_link)

