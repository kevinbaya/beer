from repres.recommenderdata import *
import numpy as np
graph_data = get_graph_data('data/mini.relation','data/mini.output',4)

def compute_reput(rating_mat,k):
	n,m = rating_mat.shape
	c = np.array([1]*n)
	
	nb_ite = 100
	i = 0
	while i<nb_ite:
		s = np.average(a=rating_mat,weights=c,axis=0)
		dist = rating_mat - s
		c= 1-k/float(m) *np.sum((dist*dist),axis=1)
		i = i + 1
	return c,s
	
if __name__=="__main__":

	rm= np.array([[3.3,4.2],[3.4,4.5],[4.9,2.8]])
	print compute_reput(rm,0.2)
		
	
