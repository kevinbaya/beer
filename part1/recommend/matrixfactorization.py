import numpy.random as npr
import numpy as np
from random import randrange
from squarematfact import *

class Gradient(object):  
	def __init__(self, train_mat):
		self.r=train_mat
		self.n, self.m = train_mat.shape
		""" Liste des indices ou la note est differente de 0 """
		self.i_list, self.j_list = np.where(self.r <> 0)

	def train(self, nb_ite):
		ite =0
		nb_ind = len(self.i_list)
		while ite < nb_ite:
			ind = randrange(nb_ind)
			i = self.i_list[ind]
			j = self.j_list[ind]
			self.pre_compute(i,j)
			self.compute_gradient(i,j) # compute_gradient nondefini
			self.update_gradient(i,j) # update_gradient non defini
			ite+=1
			if False:
				print "L'erreur sur la matrice d'apprentissage est %f"%self.cost(self.r)
		

        def error(self, test_mat):
                # print "call error rate"
		mask = test_mat <> 0
		r_pred = mask * self.all_score() # allScore non defini
		nb_misclassified = (r_pred * test_mat <0).sum()
		nb_total = mask.sum()
                return nb_misclassified/float(nb_total)
        
        def conf_mat(self,test_mat, threshold):
                mask = test_mat <> 0
                score = self.all_score()
                mask_pos = score > threshold
                mask_neg = score <= threshold
		r_pred = mask * score
		nb_tp = (r_pred * mask_pos * test_mat >0).sum()
                nb_fp = (r_pred * mask_pos * test_mat <0).sum()
                nb_tn = (r_pred * mask_neg * test_mat >0).sum()
                nb_fn = (r_pred * mask_neg * test_mat <0).sum()
                
                conf_mat = np.zeros((2,2))
                conf_mat[0,0] = nb_tp
                conf_mat[0,1] = nb_fn
                conf_mat[1,0] = nb_fp
                conf_mat[1,1] = nb_tn
                return conf_mat
                

class AbsBiasPred(object):
	def __init__(self,epsi_bu,epsi_bp,lamb_bu,lamb_bp):
		self.bu = npr.randn(self.n,1)
		self.bp = npr.randn(self.m)
		self.mu = self.r.mean()
		self.epsi_bu = epsi_bu
		self.epsi_bp = epsi_bp
		self.lamb_bu = lamb_bu
		self.lamb_bp = lamb_bp

	def score(self,i,j):
		return self.bu[i] + self.bp[j] + self.mu

	def all_score(self):
		return np.tile(self.bu,[1,self.m]) + np.tile(self.bp,[self.n,1]) + self.mu

	def compute_gradient(self,i,j) :
		self.bu_inew = self.bu[i] - self.epsi_bu *\
			(self.gradient_bu(i,j) + self.lamb_bu * self.bu[i]) # gradient_bu non defini
		self.bp_jnew = self.bp[j] - self.epsi_bp *\
			(self.gradient_bp(i,j) + self.lamb_bp * self.bp[j]) # gradient_bp non defini

	def update_gradient(self,i,j):
		self.bu[i] = self.bu_inew
		self.bp[j] = self.bp_jnew

class AbsSVD(object):

	def __init__(self, num_of_feat,epsi_u,epsi_p,lamb_u,lamb_p):
		self.f = num_of_feat
		self.u = npr.randn(self.n, self.f)
		self.p = npr.randn(self.f, self.m)
		self.epsi_u = epsi_u
		self.epsi_p = epsi_p
		self.lamb_u = lamb_u
		self.lamb_p = lamb_p

	def score(self, i,j):
		return (self.u[i,:] * self.p[:,j]).sum()

	def all_score(self):
		return np.dot(self.u,self.p)

	def compute_gradient(self,i,j):
		self.u_inew = self.u[i,:] - self.epsi_u *\
			(self.gradient_u(i,j) + self.lamb_u * self.u[i,:]) # gradient_u non defini
		self.p_jnew = self.p[:,j] - self.epsi_p *\
			(self.gradient_p(i,j) + self.lamb_p *self.p[:,j]) # gradient p non defini

	def update_gradient(self,i,j):
		self.u[i,:] = self.u_inew
		self.p[:,j] = self.p_jnew

class AbsBiasSVD(AbsSVD,AbsBiasPred):
	def __init__(self, num_of_feat, epsi_u,epsi_p,epsi_bu,epsi_bp,lamb_u,lamb_p,lamb_bu, lamb_bp):
		AbsSVD.__init__(self,num_of_feat,epsi_u,epsi_p,lamb_u,lamb_p)
		AbsBiasPred.__init__(self,epsi_bu,epsi_bp,lamb_bu,lamb_bp)

	def score(self,i,j):
		return AbsSVD.score(self,i,j)+AbsBiasPred.score(self,i,j)

	def all_score(self):
		return AbsSVD.all_score(self)+ AbsBiasPred.all_score(self)

	def compute_gradient(self,i,j):
		AbsSVD.compute_gradient(self,i,j)
		AbsBiasPred.compute_gradient(self,i,j)

	def update_gradient(self,i,j):
		AbsSVD.update_gradient(self,i,j)
		AbsBiasPred.update_gradient(self,i,j)

class BiasPred(Gradient,AbsBiasPred):
	def __init__(self,train_mat,epsi_bu,epsi_bp,lamb_bu=0,lamb_bp=0):
		Gradient.__init__(self,train_mat)
		AbsBiasPred.__init__(self,epsi_bu,epsi_bp,lamb_bu,lamb_bp)

class SVD(Gradient,AbsSVD):
	def __init__(self, train_mat, num_of_feat,epsi_u,epsi_p,lamb_u=0,lamb_p=0):
		Gradient.__init__(self,train_mat);
		AbsSVD.__init__(self,num_of_feat,epsi_u,epsi_p,lamb_u,lamb_p)

class BiasSVD(Gradient,AbsBiasSVD):
	def __init__(self, train_mat, num_of_feat, epsi_u,epsi_p,epsi_bu,epsi_bp, lamb_u,lamb_p,lamb_bu, lamb_bp):
		Gradient.__init__(self,train_mat);
		AbsBiasSVD.__init__(self, num_of_feat, epsi_u,epsi_p,epsi_bu,epsi_bp,lamb_u,lamb_p,lamb_bu, lamb_bp)
