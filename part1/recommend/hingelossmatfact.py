import numpy as np
from matrixfactorization import *
class HingelossSVD(SVD):

    def cost(self, test_mat):
        r_pred= (test_mat <> 0) *  BiasSVD.all_score(self)
        dist = np.maximum(0,1 - test_mat * r_pred)
        return dist.sum()

    def gradient_u(self, i,j):
        if self.test:
            return - self.r[i,j] * self.p[:,j]
        else:
            return 0.0
    
    def gradient_p(self, i,j):
        if self.test:
            return - self.r[i,j] * self.u[i,:]
        else:
            return 0.0

    def pre_compute(self,i,j):
        self.test = self.r[i,j] * self.score(i,j)<1

class HingelossBias(BiasPred):

	def cost(self, test_mat):
		r_pred= (test_mat <> 0) * BiasPred.all_score(self)
		dist = np.maximum(0,1 - test_mat * r_pred)
		return dist.sum()

	def pre_compute(self,i,j):
		self.test = self.r[i,j] * self.score(i,j)<1

	def gradient_bu(self,i,j):
		if self.test :
			return -self.r[i,j]
		else:
			return 0.0

	def gradient_bp(self,i,j):
		if self.test :
			return -self.r[i,j]
		else:
			return 0.0
