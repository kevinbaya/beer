import numpy as np
from matrixfactorization import *


class Hinge(object):
    def cost(self,test_mat):
        elem_cost = np.max(0, 1 - test_mat * self.all_score())
        return elem_cost.sum()

    def pre_compute(self,i,j):
        self.is_misclassified = self.r[i,j] * self.score(i,j)<1


class HingeBias(Hinge,BiasPred):

    def gradient_bu(self,i,j):
        return self.is_misclassified *(- self.r[i,j] )

    def gradient_bp(self,i,j):
        return self.is_misclassified *(- self.r[i,j] )

    def all_score(self):
        return BiasPred.all_score(self)

class HingeSVD(Hinge,SVD):

    def gradient_u(self,i,j):
        return self.is_misclassified *(- self.r[i,j] * self.p[:,j] )

    def gradient_p(self,i,j):
        return self.is_misclassified *(- self.r[i,j] * self.u[i,:] )

    def all_score(self):
        return BiasPred.all_score(self)

class HingeBiasSVD(Hinge,BiasSVD):

    def gradient_u(self,i,j):
        return self.is_misclassified *(- self.r[i,j] * self.p[:,j] )

    def gradient_p(self,i,j):
        return self.is_misclassified *(- self.r[i,j] * self.u[i,:] )

    def gradient_bu(self,i,j):
        return self.is_misclassified *(- self.r[i,j] )

    def gradient_bp(self,i,j):
        return self.is_misclassified *(- self.r[i,j] )

    def all_score(self):
        return BiasSVD.all_score(self)
    
