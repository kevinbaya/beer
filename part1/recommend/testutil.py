import numpy as np

def crossval(classmodel, rating_mat, nb_fold, params, nb_ite):
    err = np.zeros(nb_fold)
    n, m = rating_mat.shape
    np.random.shuffle(rating_mat)
    np.random.shuffle(rating_mat.T)
    ind = (np.arange(n*m)%nb_fold).reshape(n,m)
    for i in range(nb_fold):
        train_mat, test_mat = ((ind == i) * rating_mat,(ind <> i) * rating_mat)
        params["train_mat"] = train_mat
        model = classmodel(**params)
        model.train(nb_ite)
        err[i] = model.error(test_mat)
    return err

def compute_roc(classifier,test_mat, start=-1,end=1,step=0.1):
    threshold = start
    x = []
    y = []
    while threshold < end:
        conf_mat = classifier.conf_mat(test_mat,threshold)
        norm = conf_mat.sum(axis=1)
        x.append(conf_mat[1,0]/norm[1])
        y.append(conf_mat[0,0]/norm[0])

        threshold += step

    return np.array(x), np.array(y)
