import matplotlib.pyplot as plt
from testutil import *


def plot_roc(x,y):
    plt.plot(np.array([0.0,1.0]),"r")
    plt.plot(x,y,"b")
    plt.show()
