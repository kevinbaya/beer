import numpy as np
from matrixfactorization import *


class Square(object):
	def cost(self, test_mat):
		r_pred = (test_mat <> 0) * self.all_score()
		dist = np.power(test_mat - r_pred, 2)
		return dist.sum()

	def pre_compute(self,i,j):
		self.err = - self.r[i,j] + self.score(i,j)

class SquareSVD(Square,SVD):
	def gradient_u(self, i,j):
		return self.err* self.p[:,j]

	def gradient_p(self, i,j):
		return self.err* self.u[i,:]

	def all_score(self):
		return SVD.all_score(self)

class SquareBias(Square,BiasPred):

	def gradient_bu(self,i,j):
		return self.err

	def gradient_bp(self,i,j):
		return self.err

	def all_score(self):
		return BiasPred.all_score(self)

class SquareBiasSVD(Square,BiasSVD):
	def gradient_u(self, i,j):
		return self.err* self.p[:,j]

	def gradient_p(self, i,j):
		return self.err* self.u[i,:]

	def gradient_bu(self,i,j):
		return self.err

	def gradient_bp(self,i,j):
		return self.err

	def all_score(self):
		return BiasSVD.all_score(self)
	
