import numpy as np
from squarematfact import *
class RegularizedSquare(SquareMatFact):

    def __init__(self, training_matrix, num_of_feat, lamb):
        SquareMatFact.__init__(self,training_matrix, num_of_feat)
        self.lamb = lamb
    
    def cost(self, test_mat):
        reg = self.lamb *(np.power(self.u,2).sum() +np.power(self.p,2).sum())
        return SquareMatFact.cost(self,test_mat) + reg

    def gradient_u(self, i,j):
        return SquareMatFact.gradient_u(self,i,j) + self.lamb * self.u[i,:]

    
    def gradient_p(self, i,j):
        return SquareMatFact.gradient_p(self,i,j) + self.lamb * self.p[:,j]
