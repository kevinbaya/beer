import numpy as np
from random import random

class RecommenderData(object):
    
    def __init__(self):
        self.rev_to_beer= {}
        self.rev_to_user= {}
        
        self.rev_to_notes ={}
        self.user_dict = {}
        self.user_list =[]
        
        self.beer_dict = {}
        self.beer_list = []
        


    def fetch_relation(self,relation_file_name):
        file = open(relation_file_name,'r')
        for line in file:
            # R3520 B57110 review_to_beer
            tokens = line.split(' ')
            rev_id = tokens[0]
            if tokens[2][:-1] == 'review_to_beer':
                self.rev_to_beer[rev_id]=tokens[1]
                if tokens[1] not in self.beer_dict:
                    self.beer_dict[tokens[1]]=len(self.beer_list)
                    self.beer_list.append(tokens[1])
                    
            else:
                self.rev_to_user[tokens[0]]=tokens[1]
                if tokens[1] not in self.user_dict:
                    self.user_dict[tokens[1]]=len(self.user_list)
                    self.user_list.append(tokens[1])
         

    def fetch_notes(self,output_file_name):
        file = open(output_file_name,'r')
        for line in file:
            tokens= line.split(' ')
            notes=[]
            for token in tokens[2:]:
                note = token.split(':')[1]
                notes.append(int(note))
            self.rev_to_notes[tokens[0]]=notes

    def compute_matrix(self, critere):
        n=len(self.user_list)
        m=len(self.beer_list)
        self.rating_mat = np.zeros(n*m).reshape((n,m))
        for rev, notes in self.rev_to_notes.iteritems():
            if rev in self.rev_to_beer and rev in self.rev_to_user:
                i = self.user_dict[self.rev_to_user[rev]]
                j = self.beer_dict[self.rev_to_beer[rev]]
                self.rating_mat[i,j]= notes[critere]

    


    def split_data(self,percent):
        n, m = self.rating_mat.shape
        mat1 =  np.zeros(n*m).reshape((n,m))
        mat2 = np.zeros(n*m).reshape((n,m))
        
        i=0
        while i<n:
            j=0
            while j<m:
                if random()<percent :
                    mat1[i,j] = self.rating_mat[i,j]
                else:
                    mat2[i,j] = self.rating_mat[i,j]
                j+=1
            i+=1
        return mat1, mat2
        
def create_rating_mat(rel_filename,note_filename,critere):
	rm = RecommenderData()
	rm.fetch_relation(rel_filename)
	rm.fetch_notes(note_filename)
	rm.compute_matrix(critere)
	return rm


    


