from repres.recommenderdata import *
from recommend.squarematfact import *
from recommend.hingematfact import *
from recommend.testutil import *
from recommend.testutil import *
from recommend.plotutil import *
import matplotlib.pyplot as plt
import cProfile

if __name__=="__main__":
	rm=RecommenderData()
	rm.fetch_relation("data/mini.relation")
	rm.fetch_notes("data/mini.output")
	rm.compute_matrix(4)
        """
	train_mat,test_mat = rm.split_data(0.5)
	#model = SquareBias(train_mat=train_mat,epsi_bu=0.095, epsi_bp=0.0075,lamb_bu=1.6,lamb_bp=0.0001)
	# model = SquareSVD(train_mat = train_mat, num_of_feat = 3, epsi_u =0.11, epsi_p = 0.05,lamb_u = 0.24, lamb_p=0.24)
        # model = SquareBiasSVD(train_mat = train_mat, num_of_feat = 3, epsi_u =0.11, epsi_p = 0.05,lamb_u = 0.24, lamb_p=0.24, epsi_bu=0.095, epsi_bp=0.0075,lamb_bu=1.6,lamb_bp=0.0001)
        model = HingeBias(train_mat=train_mat,epsi_bu=0.095, epsi_bp=0.0075,lamb_bu=1.6,lamb_bp=0.0001)
	model.train(nb_ite = 10**6)
        x, y = compute_roc(model, test_mat)
        plot_roc(x,y)
        plt.show()
	print model.error(test_mat)
        """
        
        nb_fold = 10
        nb_ite= 10**6
        params=dict(epsi_bu=0.095, epsi_bp=0.0075, lamb_bu=1.6, lamb_bp=0.0001)
	for par in [0.005,0.01,0.05,0.1,0.2]:
                update="epsi_bu"
                params[update] = par
                err = crossval(HingeBias, rm.rating_mat,
                               nb_fold,params, nb_ite)
                print "L'erreur est cross validation pour %s = %f est %f" % (update,par,np.mean(err))


        """
        nb_fold = 2
        nb_ite= 10**5
        params=dict(epsi_bu=0.095, epsi_bp=0.0075,
                    lamb_bu=1.6, lamb_bp=0.0001)
        cProfile.run('crossval(HingeBias, rm.rating_mat,nb_fold,params, nb_ite)')
        """
