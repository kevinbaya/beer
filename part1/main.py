from repres.reviewset import *
from repres.review import *
from classif.perceptron import *
from classif.hingeloss import *
from classif.evaluate import *
import math

def write_word_cloud(indexfile,classifier,n):
    infile = open(indexfile,"r")
    outfile = open("./wordcloud.txt","w+")
    sorted_w = sorted(range(len(classifier.w)), key=lambda k: -classifier.w[k])[:n]
    denom = math.sqrt(sum(w_i*w_i for w_i in classifier.w))
    poids = [w_i/denom for w_i in classifier.w]
    for line in infile:
        tokens = line.split(' ')
        i = int(tokens[1])
        try:
            ind = sorted_w.index(i)
        except ValueError, e:
            ind=-1
       
        if ind > -1 :
            outfile.write('%s:%f\n'%(tokens[0],poids[sorted_w[ind]]))



if __name__ == "__main__":
    
    revs = ReviewSet()
    revs.parse_vectors('data/medium.input',True)
    revs.parse_notes('data/medium.output')
    
    '''
    perceptron = Perceptron(DIMENSION)
    ev = Evaluate(classifier=perceptron, review_set=revs)
    print ev.split_learn_test(0.5, 80, 0.01)
    '''
    
    

    '''
    hingeloss = Hingeloss(DIMENSION)
    ev = Evaluate(classifier=hingeloss, review_set=revs)
    print ev.split_learn_test(0.5, 80, 0.001)
    '''
    
    '''
    print Evaluate.test(model=Hingeloss,
                        size_list = [10000,30000,50000,75000,100000],
                        ite_list = [50,100],
                        epsi_list = [0.001,0.01,0.1],
                        review_set = revs,
                        nb_repet = 5,
                        note = 4 )
    '''

    hingeloss = Hingeloss(DIMENSION)
    hingeloss.train(train_set=revs, nb_ite=80, epsi=0.001)
    write_word_cloud("data/medium.index_words",hingeloss,10)
    
    
