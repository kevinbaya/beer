from perceptron import *
from repres.reviewset import *
from repres.review import *
from hingeloss import *
import numpy as np



class Evaluate(object):

    def __init__(self,classifier,review_set):
        self.classifier = classifier
        self.review_set = review_set

    def split_learn_test(self, percent,nb_ite,epsi,note=4):
        train_set,test_set = self.review_set.split_data(percent)
        self.classifier.train(train_set,nb_ite,epsi)
        return Evaluate.get_error_rate(self.classifier,test_set,note)

    @staticmethod
    def get_error_rate(classifier,test_set,note):

        def is_wrong(rev):
            return classifier.f(rev) * rev.notes[note] <0

        cpt = len(filter(is_wrong,test_set.rev_list))
        return float(cpt)/len(test_set.rev_list)
    
    @staticmethod
    def test( size_list, ite_list, epsi_list, review_set,model=Hingeloss, nb_repet=10,note=4):
        err= 1-np.zeros([len(size_list),len(ite_list),len(epsi_list)]) 
        for i in range(len(size_list)):
            percent = size_list[i]/float(len(review_set.rev_list))

            for j in range(len(ite_list)):
                for k in range(len(epsi_list)):
                    err_vect= 1 - np.zeros(nb_repet)

                    for l in range(nb_repet):
                        train_set, test_set = review_set.split_data(percent)
                        classifier = model(DIMENSION,note)
                        classifier.train(train_set,ite_list[j],epsi_list[k])
                        err_vect[l]= Evaluate.get_error_rate(classifier,test_set,note)
                    err[i,j,k] =err_vect.mean()

        return err
