from abc import ABCMeta, abstractproperty

# Classe abstraite que doit implementer un classifeur
class Classifier(object):
    __metaclass__=ABCMeta
    @abstractproperty
    def train(app_set):
        pass

    @abstractproperty
    def f(rev):
        pass

