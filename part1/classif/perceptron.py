from random import random,randrange
from classifier import Classifier
from evaluate import *
import random

class Perceptron(Classifier):
    
    def __init__(self,dimension):
        self.w = [0]*(dimension)
        self.bias = 1

    def train(self, train_set,nb_ite,epsi):
        ite=0
        while ite<nb_ite:
            j=0
            while j< len(train_set.rev_list):
                x = train_set.rev_list[j]
                t = x.notes[4]
                fx = self.f(x)
                if (fx*t < 0):
                    for i,x_i in x.vector.iteritems():
                        self.w[i] -= epsi* x_i * fx
                        self.bias -= epsi * fx
                j+=1
            print "Le cout sur la base d'app est %f" % self.cost(train_set)
            ite+=1

    def f(self,rev):
        y=0
        for k,v in rev.vector.iteritems():
            y+=self.w[k]*v
        return y+self.bias

    def cost(self,train_set):
        i = 0
        res = 0.0
        while (i<len(train_set.rev_list)):
            x =  train_set.rev_list[i]
            t = train_set.rev_list[i].notes[4]
            res += max(0, -t* self.f(x))
            i+=1
        return res
