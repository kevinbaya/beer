from random import random,randrange
from classifier import Classifier
from evaluate import *

class Hingeloss(Classifier):
    
    def __init__(self,dimension,note = 4):
        self.note = 4
        self.w = [0]*(dimension)
        self.bias = 1

    def train(self, train_set,nb_ite,epsi):
        ite=0
        while ite<nb_ite:
            j=0
            while j< len(train_set.rev_list):
                x = train_set.rev_list[j]
                y = x.notes[self.note]
                fx = self.f(x)
                if (fx*y < 1):
                    for i,x_i in x.vector.iteritems():
                        self.w[i] += epsi* x_i * y
                        self.bias += epsi * y
                j+=1
            #if ite%(nb_ite/10) ==0:
            #    print "Le cout sur la base d'app est %f" % self.cost(train_set)
            ite+=1

    def f(self,rev):
        y=0
        for k,v in rev.vector.iteritems():
            y+=self.w[k]*v
        return y+self.bias

    def cost(self,train_set):
        i = 0
        res = 0.0
        while (i<len(train_set.rev_list)):
            x =  train_set.rev_list[i]
            t = train_set.rev_list[i].notes[self.note]
            res += max(0, 1-t* self.f(x))
            i+=1
        return res
